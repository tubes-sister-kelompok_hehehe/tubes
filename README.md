# TUGAS BESAR - SISTEM PARALEL DAN TERDISTRIBUSI

**ATM by Socket/Socket ATM**

ATM (*Automatic Teller Machine*) adalah sebuah alat elektronik yang melayani nasabah bank untuk mengambil uang dan mengecek rekening tabungan mereka tanpa perlu dilayani oleh seorang "*teller*" manusia. Banyak ATM juga melayani penyimpanan uang atau cek, transfer uang atau bahkan membeli pulsa telepon seluler. Dalam tugas besar ini, kami menggunakan socket dan sudah terdistribusi ,menyediakan 6 fitur di dalamnya ,diantaranya login ,cek saldo, tarik tunai, transfer, setor tunai, lihat akun, dan ganti akun.

**Daftar Anggota Kelompok**
1. Anom Sentanu Prayosa
2. Fitri Eka Cahyanti
3. Fetra Moira Fiermansyah
4. Rizaldo Laksana

**Detail Socket**
1. IP : 192.168.1.15
2. PORT : 12345

Fitur Baru dan Commite
1. Login
    Pada login terdapat pin yang harus terlebih dahulu diinputkan kepada sistem. apabila pin yang dimasukans esuai dengan pin yang terdapat pada data.csv maka akan masuk kedalam menu utama untuk dapat menikmati aplikasi ATM ini.
    ![](https://gitlab.com/tubes-sister-kelompok_hehehe/tubes/raw/master/ss/tampilan_utama_.png)

2. Ganti PIN (Anom Sentanu Prayosa)
    Pada fitur ini, pengguna ATM dapat mengganti pin yang digunakan untuk melakukan login pada ATM yang sudah terdaftar.
    ![](https://gitlab.com/tubes-sister-kelompok_hehehe/tubes/raw/master/ss/ubah_pin.png)

3. Cek Saldo
    Fitur cek saldo digunakan untuk melihat sisa saldo yang dimiliki oleh akun yang sedang login kedalam sistem.
    ![](https://gitlab.com/tubes-sister-kelompok_hehehe/tubes/raw/master/ss/cek_saldo.png)

4. Tarik Tunai 
    Tarik tunai atau withdraw berfungsi untuk mengambil uang sesuai dengan jumlah yang diinginkan oleh pengguna dan mengurangi saldo yang dimiliki pengguna dan disimpan kembali pada sistem.
    ![](https://gitlab.com/tubes-sister-kelompok_hehehe/tubes/raw/master/ss/tarik_tunai.png)

5. Setor tunai (Fitri Eka Cahyanti)
    Dalam fitur ini, pengguna melakukan deposit ke akun dan jumlah uang yang di setor akan disimpan oleh sistem dengan menambahkannya dengan sisa saldo sebelum deposit.

    ![](https://gitlab.com/tubes-sister-kelompok_hehehe/tubes/raw/master/ss/setor_tunai.png)

6. Transfer (Rizaldo Laksana)
    Pengguna akan melakukan transfer uang ke akun lain dengan memasukkan pin yang dimiliki oleh akun lain dan nominal uang yang akan ditransfer. Saldo pengguna akan dikurangi sejumlah uang transfer dan saldo penerima akan bertambah sesuai jumlah transfer

    ![](https://gitlab.com/tubes-sister-kelompok_hehehe/tubes/raw/master/ss/transfer.png)

7. Lihat akun (Fetra Moira Fiermansyah)
    Fitur ini digunakan untuk melihat data akun pengguna yang disimpan oleh sistem pada file csv. Akan ditampilkan pin, nama, umur, jenis kelamin, dan saldo pengguna.

    ![](https://gitlab.com/tubes-sister-kelompok_hehehe/tubes/raw/master/ss/lihat_akun.png)