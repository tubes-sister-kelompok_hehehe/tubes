import time
import socket
import csv
from tempfile import NamedTemporaryFile
import shutil

s = socket.socket()
ip = "192.168.1.15"
port = 12345
Buffer = 1024
message = 'hehe'
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((ip, port))
sock.send(str.encode(message))
data = sock.recv(Buffer)
print(data.decode())
sock.close()


class ATM:

    def ben(self):
        global row, pin
        print('harap tunggu...')
        time.sleep(2)
        filename = 'data.csv'
        tempfile = NamedTemporaryFile(mode='w', delete=False)
        fields = ['PIN','name','gender','age','saldo']
        with open(filename, 'r') as csvfile, tempfile:
                reader = csv.DictReader(csvfile, fieldnames=fields)
                for row in reader:
                    if row['PIN'] == pin:
                        print('saldo anda saat ini : ', row['saldo'])
        time.sleep(2)
        atm.menu()

    def wdr(self):
        global row, pin
        data = int(row['saldo'])
        print('\n\nharap tunggu...')
        time.sleep(3)
        withdraw = int(input('ketik dana yang ingin ditarik : '))
        if withdraw <= data:
            data -= withdraw
            row['saldo'] = str(data)
            filename = 'data.csv'
            tempfile = NamedTemporaryFile(mode='w', delete=False)
            fields = ['PIN','name','gender','age','saldo']
            with open(filename, 'r') as csvfile, tempfile:
                reader = csv.DictReader(csvfile, fieldnames=fields)
                writer = csv.DictWriter(tempfile, fieldnames=fields)
                for row in reader:
                    if row['PIN'] == pin:
                        row['saldo'] = str(data)
                    else:
                        row = {'PIN': row['PIN'], 'name': row['name'], 'gender': row['gender'], 'age': row['age'],
                               'saldo': row['saldo']}
                    writer.writerow(row)
            shutil.move(tempfile.name, filename)
            choice = input('tekan Y/y, jika anda ingin cek saldo. jika tidak tekan tombol lainnya : ')
            if choice == 'Y' or choice == 'y':
                print('saldo anda : ', str(data))
        else:
            print('saldo anda tidak mencukupi , silahkan ketik dibawah nominal ', data)
        print('\n\nkembali ke main menu')
        time.sleep(3)
        self.menu()

    def setor_tunai(self):
        global row, pin
        data = int(row['saldo'])
        print('\n\nharap tunggu...')
        time.sleep(3)
        setor = int(input('ketik dana yang ingin ditambah : '))
        if setor >= 0 :
            data += setor
            row['saldo'] = str(data)
            filename = 'data.csv'
            tempfile = NamedTemporaryFile(mode='w', delete=False)
            fields = ['PIN','name','gender','age','saldo']
            with open(filename, 'r') as csvfile, tempfile:
                reader = csv.DictReader(csvfile, fieldnames=fields)
                writer = csv.DictWriter(tempfile, fieldnames=fields)
                for row in reader:
                    if row['PIN'] == pin:
                        row['saldo'] = str(data)
                    else:
                        row = {'PIN': row['PIN'], 'name': row['name'], 'gender': row['gender'], 'age': row['age'],
                               'saldo': row['saldo']}
                    writer.writerow(row)
            shutil.move(tempfile.name, filename)
            choice = input('tekan Y/y, jika anda ingin cek saldo. jika tidak tekan tombol lainnya : ')
            if choice == 'Y' or choice == 'y':
                print('saldo anda : ', str(data))
        else:
            print('saldo anda tidak mencukupi , silahkan ketik dibawah nominal ', data)
        print('\n\nkembali ke main menu')
        time.sleep(3)
        self.menu()

    def lihat_akun (self):
        global row, pin
        filename = 'data.csv'
        tempfile = NamedTemporaryFile(mode='w', delete=False)
        fields = ['PIN','name','gender','age','saldo']
        with open(filename, 'r') as csvfile, tempfile:
            reader = csv.DictReader(csvfile, fieldnames=fields)
            for row in reader:
                if row['PIN'] == pin:
                    print( 'nama : ', row['name'] ,'\n' , 'saldo : ', row ['saldo'],'\n' , 'gender : ', row ['gender'],'\n' , 'age : ', row ['age'] )
        print('\n\nkembali ke main menu')
        time.sleep(3)
        self.menu()
    
    def nilaiQ(self):
        global kode,nilai
        filename = 'data.csv'
        tempfile = NamedTemporaryFile(mode='w', delete=False)
        fields = ['PIN','name','gender','age','saldo']
        with open(filename, 'r') as csvfile, tempfile:
            reader = csv.DictReader(csvfile, fieldnames=fields)
            writer = csv.DictWriter(tempfile, fieldnames=fields)
            #print('\n\nharap tunggu...')
            time.sleep(3)
            for row in reader:
                if (row['PIN'] == kode) :
                    print(kode)
                    data = int(row['saldo'])
                    data += nilai
                    row['saldo'] = str(data)
                    print(row['saldo'])
                    print('ini nilai')
                    writer.writerow(row)
        shutil.move(tempfile.name, filename)

    def transfer (self):
        global row,pin,nilai, kode
        kode = input('Masukan nama account yang akan di transfer : ')
        nilai = int(input('Masukan uang : '))
        print('\n\nharap tunggu...')
        time.sleep(3)
        data = int(row['saldo'])
        data -= nilai
        row['saldo'] = str(data)
        filename = 'data.csv'
        tempfile = NamedTemporaryFile(mode='w', delete=False)
        fields = ['PIN','name','gender','age','saldo']
        with open(filename, 'r') as csvfile, tempfile:
            reader = csv.DictReader(csvfile, fieldnames=fields)
            writer = csv.DictWriter(tempfile, fieldnames=fields)
            for row in reader:
                if pin == row['PIN']:
                    row['saldo'] = str(data)
                    self.nilaiQ()
                else:
                    row = {'PIN': row['PIN'], 'name': row['name'], 'gender': row['gender'], 'age': row['age'],
                               'saldo': row['saldo']}
                writer.writerow(row)
        shutil.move(tempfile.name, filename)
        choice = input('tekan Y/y, jika anda ingin cek saldo. jika tidak tekan tombol lainnya : ')
        if choice == 'Y' or choice == 'y':
            print('saldo anda : ', str(data))
        else:
            print('saldo anda tidak mencukupi , silahkan ketik dibawah nominal ', data)
        print('\n\nkembali ke main menu')
        time.sleep(3)
        self.menu()

                    
    def menu(self):
        print('  \n1. Cek Saldo  \n2. Tarik Tunai \n3. Transfer \n4. Setor Tunai \n5. Lihat Akun \n6. ganti pin \n7. keluar' )
        ch = input('ketik pilihan: ')
        if ch == '1':
            self.ben()
        elif ch == '2':
            self.wdr()
        elif ch == '3':
            self.transfer()
        elif ch == '4':
            self.setor_tunai()
        elif ch == '5':
            self.lihat_akun()
        elif ch == '6':
            self.ganti_pin()
        elif ch == '7':
            exit()
        else:
            print('pilih dengan benar : ')
            atm.menu()

    def ganti_pin(self):
        global row
        data = input('masukan pin : ')
        filename = 'data.csv'
        tempfile = NamedTemporaryFile(mode='w', delete=False)
        fields = ['PIN','name','gender','age','saldo']
        with open(filename, 'r') as csvfile, tempfile:
            reader = csv.DictReader(csvfile, fieldnames=fields)
            writer = csv.DictWriter(tempfile, fieldnames=fields)
            for row in reader:
                if pin == row['PIN']:
                    row['PIN'] = str(data)
                else:
                    row = {'PIN': row['PIN'], 'name': row['name'], 'gender': row['gender'], 'age': row['age'],
                               'saldo': row['saldo']}
                writer.writerow(row)
        shutil.move(tempfile.name, filename)
        print('\n\nkembali ke main menu')
        time.sleep(3)
        self.menu()

    def buat_akun(self):
        filename = 'data.csv'
        tempfile = NamedTemporaryFile(mode='a', delete=False)
        fields = ['PIN','name','gender','age','saldo']
        #filename = 'data.csv'
        with open(filename, 'r') as csvfile, tempfile:
            reader = csv.DictReader(csvfile, fieldnames=fields)
            writer = csv.DictWriter(tempfile, fieldnames=fields)
            for row in reader:
                if (index >=  0):
                    row['PIN']  = input ('masukan pin : ')
                    row['name'] = input ('masukan nama : ')
                    #data_n.append(b)
                    row['gender'] = input ('Masukan gender : ')
                    #data_n.append(c)
                    row ['age'] = input ('Masukan age : ')
                    #data_n.append(d)
                    row['saldo'] = input('masukkan saldo awal : ')
            #ilecsv = open(file)
            writer.writerows(row)
        shutil.move(tempfile.name, filename)
        print('\n\nkembali ke main menu')
        time.sleep(3)
        self.menu()

    def login(self):
        global row, file, pin
        index = 0
        ch = input('Apakah anda memiliki kartu Y/N     : ')
        if ch == 'Y' or ch == 'y' : 
            pin = input('masukkan PIN : ')
            time.sleep(2)
            with open('data.csv') as file:
                reader = csv.DictReader(file)
                for row in reader:
                    index += index
                    if row['PIN'] == pin :
                        print('kartu sukses dijalankan')
                        atm.menu()
                else:
                    self.login()
                    index += index
        elif ch == 'N' or ch == 'n':
            print('BUAT AKUN')
            self.buat_akun()
print('kartu masuk')
time.sleep(3)
atm = ATM()
atm.login()